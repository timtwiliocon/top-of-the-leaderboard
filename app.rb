require 'httparty'
require 'json'
require 'sinatra'

# This app was built for the Hacker Olympics at TwilioCon 3 by:
#
# Tim
#
# Emil been here... !


get '/' do
  response = HTTParty.get('http://judging.thehackerolympics.com/leaderboard.json').
    body
  json = JSON.parse(response)

  @winner = json.values.sort_by! { |v| v["points"]}.last
  erb :index
end